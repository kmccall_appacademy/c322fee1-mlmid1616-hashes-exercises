# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  word_lengths = Hash.new(0)
  str.split.each do |word|
    word_lengths[word] = word.length
  end
  word_lengths
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by do |k,v| v end.last[0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |k,v| older[k] = v end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  word_count = Hash.new(0)
  word.chars.each do |letter|
    word_count[letter] += 1
  end
  word_count
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  uniq_array = Hash.new
  arr.each_with_index do |el,idx|
    uniq_array[idx] = el unless uniq_array.has_value?(el)
  end
  uniq_array.values
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  even_or_odd = Hash.new(0)
  numbers.each do |num|
    num.odd? ? even_or_odd[:odd] += 1 : even_or_odd[:even] += 1
  end
  even_or_odd
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)

  vowels = "aeiou"
  freq_letter = Hash.new(0)
  string.delete!("! ,.?")
  string.chars.each do |letter|
    freq_letter[letter] += 1 if vowels.include?(letter)
  end

  most_freq_vowel = freq_letter.keys.reduce do |acc, el|
    if freq_letter[el] > freq_letter[acc]
      acc = el
    elsif freq_letter[el] == freq_letter[acc] && el < acc
      acc = el
    end
    acc
    end
  most_freq_vowel

end
# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  sec_half_hash = Hash.new
  students.each do |k,v|
    sec_half_hash[k] = v if v > 6
  end

  sec_half_array = Array.new
  i = 0
  sec_half_hash.keys.each_with_index do |student1, idx1|
    sec_half_hash.keys.each_with_index do |student2,idx2|
      sec_half_array << [student1,student2] if idx2 > idx1
    end
  end
  sec_half_array
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  specie_freq = Hash.new(0)
  specimens.each do |specie|
    specie_freq[specie] += 1
  end

  number_of_species = specie_freq.keys.length
  smallest_population_size = specie_freq.values.max
  largest_population_size = specie_freq.values.min
  biodiversity_index = (number_of_species ** 2) * smallest_population_size / largest_population_size
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_sign.delete!(" !?,':")
  vandalized_sign.delete!(" !?,':")
  lower_case_normal = []
  normal_sign.chars.each do |letter| lower_case_normal << letter.downcase end
  van_hash = Hash.new(0)
  norm_hash = Hash.new(0)

  vandalized_sign.chars.each do |letter|
    van_hash[letter] += 1
  end

  lower_case_normal.each do |letter|
    norm_hash[letter] += 1
  end

  vandalized_sign.chars.all? do |letter|
    van_hash[letter.downcase] <= norm_hash[letter.downcase]
  end
end

def character_count(str)
end
